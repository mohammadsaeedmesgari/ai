module Main where

import Rules
import Control.Concurrent

render :: State -> IO ()
render state = print (show state)

progressTurn :: State -> State
progressTurn state = state { turn = turn state + 1 }

stateReducer :: State -> (State -> State) -> State
stateReducer v f = f v

reducersList :: [State -> State]
reducersList = [
        progressTurn
    ] 

loop :: State -> IO ()
loop state = do
    render state

    let reduced_state = foldl stateReducer state reducersList

    threadDelay 1000000
    loop reduced_state

main = loop initializeState
    
    
    
