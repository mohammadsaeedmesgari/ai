module Rules where

newtype State = State {
    turn :: Int
} deriving (Show)

initializeState = State { turn = 1 }