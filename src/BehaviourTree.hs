module BehaviourTree where

-- Leaf -> () -> Bool (success or not, interrupts sequence)
-- Composite -> control flow
-- Composite : Selector (pick one by random, or condition) / Sequence (one after another)
-- Decorator -> add behavior to children
-- Blackboard -> data about world and such to be used by a tree

type Composite = Blackboard -> [Node] -> Maybe Node
data Node = Root Node
          | Selector Composite [Node]
          | Sequence Composite [Node]
          | Decorator Node
          | Leaf

instance Show Node where
  show (Root _) = "root"
  show (Selector composite _) = "selector + composite"
  show (Sequence composite _) = "sequence + composite"
  show (Decorator _) = "decorator"
  show Leaf = "leaf"

newtype Blackboard = Blackboard {
    blackboardFood :: Int
}

getNext :: Blackboard -> Node -> Maybe Node
getNext bb (Root n) = Just n
getNext bb (Selector composite nodes) = composite bb nodes
getNext bb (Sequence composite nodes) = composite bb nodes
getNext bb (Decorator n) = Just n
getNext bb Leaf = Nothing
