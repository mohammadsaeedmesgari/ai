module BrainHunger where

import BehaviourTree

blackboard = Blackboard { blackboardFood = 3 }

-- TODO: create real selector based on blackboard
hungerOperation = Selector (\bb nodes -> Just (head nodes)) [Leaf, Leaf]

tree = Root hungerOperation